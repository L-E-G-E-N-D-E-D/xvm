package
{
    public class NY extends Object
    {

        public static const VEHICLEBONUSPANEL_TOOLTIP_SETCUSTOMIZATION:String = "#ny:vehicleBonusPanel/tooltip/setCustomization";

        public static const VEHICLEBONUSPANEL_TOOLTIP_SETCUSTOMIZATION_DISABLED:String = "#ny:vehicleBonusPanel/tooltip/setCustomization/disabled";

        public static const BACKBUTTON_LABEL:String = "#ny:backButton/label";

        public static const BACKBUTTON_NEWYEARCRAFTVIEW:String = "#ny:backButton/NewYearCraftView";

        public static const BACKBUTTON_NEWYEARREWARDSVIEW:String = "#ny:backButton/NewYearRewardsView";

        public static const BACKBUTTON_NEWYEARBREAKDECORATIONSVIEW:String = "#ny:backButton/NewYearBreakDecorationsView";

        public static const BACKBUTTON_ALBUMPAGE19VIEW:String = "#ny:backButton/AlbumPage19View";

        public static const BACKBUTTON_ALBUMMAINVIEW:String = "#ny:backButton/AlbumMainView";

        public static const BACKBUTTON_ALBUM19:String = "#ny:backButton/album19";

        public static const MAINVIEW_COLLECTIONBUTTON_LABEL:String = "#ny:mainView/collectionButton/label";

        public static const MAINVIEW_SIDEBAR_FIRBTN_LABEL:String = "#ny:mainView/sideBar/FirBtn/label";

        public static const MAINVIEW_SIDEBAR_TANKPARKINGBTN_LABEL:String = "#ny:mainView/sideBar/TankParkingBtn/label";

        public static const MAINVIEW_SIDEBAR_FIELDKITCHENBTN_LABEL:String = "#ny:mainView/sideBar/FieldKitchenBtn/label";

        public static const MAINVIEW_SIDEBAR_ILLUMINATIONBTN_LABEL:String = "#ny:mainView/sideBar/IlluminationBtn/label";

        public static const MAINVIEW_CRAFTBTN_LABEL:String = "#ny:mainView/craftBtn/label";

        public static const MAINVIEW_ALBUMBTN_LABEL:String = "#ny:mainView/albumBtn/label";

        public static const MAINVIEW_GETPARTSBTN_LABEL:String = "#ny:mainView/getPartsBtn/label";

        public static const TABBAR_FIR:String = "#ny:tabBar/Fir";

        public static const TABBAR_TABLEFUL:String = "#ny:tabBar/Tableful";

        public static const TABBAR_ILLUMINATION:String = "#ny:tabBar/Illumination";

        public static const TABBAR_INSTALLATION:String = "#ny:tabBar/Installation";

        public static const TABBAR_MASCOT:String = "#ny:tabBar/Mascot";

        public static const TABBAR_FORLEVELS:String = "#ny:tabBar/forLevels";

        public static const TABBAR_FORCOLLECTION:String = "#ny:tabBar/forCollection";

        public static const TABBAR_VEHICLE:String = "#ny:tabBar/vehicle";

        public static const WIDGET_TITLE:String = "#ny:widget/title";

        public static const WIDGET_REWARDSBUTTON_LABEL:String = "#ny:widget/rewardsButton/label";

        public static const WIDGET_MENU_GLADE:String = "#ny:widget/menu/glade";

        public static const WIDGET_MENU_GLADE_HEADER:String = "#ny:widget/menu/glade/header";

        public static const WIDGET_MENU_GLADE_BODY:String = "#ny:widget/menu/glade/body";

        public static const WIDGET_MENU_REWARDS:String = "#ny:widget/menu/rewards";

        public static const WIDGET_MENU_REWARDS_HEADER:String = "#ny:widget/menu/rewards/header";

        public static const WIDGET_MENU_REWARDS_BODY:String = "#ny:widget/menu/rewards/body";

        public static const WIDGET_MENU_INFO:String = "#ny:widget/menu/info";

        public static const WIDGET_MENU_INFO_HEADER:String = "#ny:widget/menu/info/header";

        public static const WIDGET_MENU_INFO_BODY:String = "#ny:widget/menu/info/body";

        public static const WIDGET_MENU_DECORATIONS:String = "#ny:widget/menu/decorations";

        public static const WIDGET_MENU_DECORATIONS_HEADER:String = "#ny:widget/menu/decorations/header";

        public static const WIDGET_MENU_DECORATIONS_BODY:String = "#ny:widget/menu/decorations/body";

        public static const WIDGET_MENU_SHARDS:String = "#ny:widget/menu/shards";

        public static const WIDGET_MENU_COLLECTIONS:String = "#ny:widget/menu/collections";

        public static const WIDGET_MENU_COLLECTIONS_HEADER:String = "#ny:widget/menu/collections/header";

        public static const WIDGET_MENU_COLLECTIONS_SUBHEADER:String = "#ny:widget/menu/collections/subheader";

        public static const WIDGET_MENU_SHARDS_INFOTEXT:String = "#ny:widget/menu/shards/infoText";

        public static const WIDGET_MENU_COLLECTIONS_BODY:String = "#ny:widget/menu/collections/body";

        public static const DECORATIONSPOPOVER_DESCRIPTION:String = "#ny:decorationsPopover/description";

        public static const DECORATIONSPOPOVER_BREAKBTN_LABEL:String = "#ny:decorationsPopover/breakBtn/label";

        public static const DECORATIONSPOPOVER_BREAKBTN_DISABLED:String = "#ny:decorationsPopover/breakBtn/disabled";

        public static const DECORATIONSPOPOVER_BREAKBTN_TOOLTIP_BODY:String = "#ny:decorationsPopover/breakBtn/tooltip/body";

        public static const DECORATIONSPOPOVER_EMPTY_TITLE:String = "#ny:decorationsPopover/empty/title";

        public static const DECORATIONSPOPOVER_EMPTY_DESCRIPTION_PART_0:String = "#ny:decorationsPopover/empty/description/part_0";

        public static const DECORATIONSPOPOVER_EMPTY_DESCRIPTION_PART_1:String = "#ny:decorationsPopover/empty/description/part_1";

        public static const DECORATIONSPOPOVER_EMPTY_DESCRIPTION_PART_2:String = "#ny:decorationsPopover/empty/description/part_2";

        public static const DECORATIONSPOPOVER_EMPTY_DESCRIPTION_PART_3:String = "#ny:decorationsPopover/empty/description/part_3";

        public static const DECORATIONSPOPOVER_BREAK_INFO:String = "#ny:decorationsPopover/break/info";

        public static const DECORATIONSPOPOVER_GOTOCRAFT_TOOLTIP:String = "#ny:decorationsPopover/goToCraft/tooltip";

        public static const MEGADECORATIONSPOPOVER_HASNOTDECORATION_FIRST:String = "#ny:megaDecorationsPopover/hasNotDecoration/first";

        public static const MEGADECORATIONSPOPOVER_HASNOTDECORATION_SECOND:String = "#ny:megaDecorationsPopover/hasNotDecoration/second";

        public static const MEGADECORATIONSPOPOVER_SELECTEDDECORATION:String = "#ny:megaDecorationsPopover/selectedDecoration";

        public static const MEGADECORATIONSPOPOVER_NOTSELECTEDDECORATION:String = "#ny:megaDecorationsPopover/notSelectedDecoration";

        public static const GETPARTSBTN_TOOLTIP_TITLE:String = "#ny:getPartsBtn/tooltip/title";

        public static const GETPARTSBTN_TOOLTIP_DESCRIPTION:String = "#ny:getPartsBtn/tooltip/description";

        public static const GETPARTSBTN_TOOLTIP_AVAILABLE:String = "#ny:getPartsBtn/tooltip/available";

        public static const GETPARTSBTN_TOOLTIP_NOTE:String = "#ny:getPartsBtn/tooltip/note";

        public static const COLLECTIONSBTN_TOOLTIP_TITLE:String = "#ny:collectionsBtn/tooltip/title";

        public static const COLLECTIONSBTN_TOOLTIP_DESCRIPTION:String = "#ny:collectionsBtn/tooltip/description";

        public static const COLLECTIONSBTN_TOOLTIP_COUNTINFO:String = "#ny:collectionsBtn/tooltip/countInfo";

        public static const COLLECTIONSBTN_TOOLTIP_NOTE:String = "#ny:collectionsBtn/tooltip/note";

        public static const DECORATIONSLOT_TOOLTIP_LEVELDECORATION:String = "#ny:decorationSlot/tooltip/levelDecoration";

        public static const DECORATIONSLOT_TOOLTIP_ATMOSPHEREPOINTS:String = "#ny:decorationSlot/tooltip/atmospherePoints";

        public static const DECORATIONSLOT_TOOLTIP_COSTPARTS_GROUP:String = "#ny:decorationSlot/tooltip/costParts/group";

        public static const DECORATIONSLOT_TOOLTIP_COSTPARTS:String = "#ny:decorationSlot/tooltip/costParts";

        public static const DECORATIONSLOT_TOOLTIP_COUNT:String = "#ny:decorationSlot/tooltip/count";

        public static const DECORATIONSLOT_TOOLTIP_BONUS:String = "#ny:decorationSlot/tooltip/bonus";

        public static const DECORATIONSLOT_TOOLTIP_MEGACOLLECTION:String = "#ny:decorationSlot/tooltip/megaCollection";

        public static const ATMOSPHERESLOT_TOOLTIP_HOLDAYATMOS:String = "#ny:atmosphereSlot/tooltip/holdayAtmos";

        public static const ATMOSPHERESLOT_TOOLTIP_ATMOSPHEREPOINTS:String = "#ny:atmosphereSlot/tooltip/atmospherePoints";

        public static const ATMOSPHERESLOT_TOOLTIP_TOYPOINTSADD:String = "#ny:atmosphereSlot/tooltip/toyPointsAdd";

        public static const ATMOSPHERESLOT_TOOLTIP_LEVEL:String = "#ny:atmosphereSlot/tooltip/level";

        public static const ATMOSPHERESLOT_TOOLTIP_NUMBEROFPOINTS:String = "#ny:atmosphereSlot/tooltip/numberOfPoints";

        public static const ATMOSPHERESLOT_TOOLTIP_REWARDSFORNETXLEVEL:String = "#ny:atmosphereSlot/tooltip/rewardsForNetxLevel";

        public static const ATMOSPHERESLOT_TOOLTIP_DISCOUNTVEHICLE:String = "#ny:atmosphereSlot/tooltip/discountVehicle";

        public static const ATMOSPHERESLOT_TOOLTIP_TANKWOMAN:String = "#ny:atmosphereSlot/tooltip/tankWoman";

        public static const CRAFTMACHINEBTN_TOOLTIP:String = "#ny:craftMachineBtn/tooltip";

        public static const ALBUMBTN_TOOLTIP:String = "#ny:albumBtn/tooltip";

        public static const TANKSLOT_TOOLTIP_HEADER:String = "#ny:tankSlot/tooltip/header";

        public static const TANKSLOT_TOOLTIP_DESCRIPTION:String = "#ny:tankSlot/tooltip/description";

        public static const TANKSLOT_TOOLTIP_SECONDARYDESCRIPTION:String = "#ny:tankSlot/tooltip/secondaryDescription";

        public static const TANKSLOT_TOOLTIP_XPFACTOR:String = "#ny:tankSlot/tooltip/xpFactor";

        public static const TANKSLOT_TOOLTIP_FREEXPFACTOR:String = "#ny:tankSlot/tooltip/freeXPFactor";

        public static const TANKSLOT_TOOLTIP_TANKMENXPFACTOR:String = "#ny:tankSlot/tooltip/tankmenXPFactor";

        public static const ALBUM_SIDEBAR_NYCOLLECTION:String = "#ny:album/sideBar/nyCollection";

        public static const ALBUM18_AVALIBLEMAXLVL:String = "#ny:album18/avalibleMaxLvl";

        public static const ALBUM18_FILLLASTYEAR:String = "#ny:album18/fillLastYear";

        public static const ALBUM19_FILLLASTYEAR:String = "#ny:album19/fillLastYear";

        public static const ALBUM_NY18_NAME:String = "#ny:album/ny18/name";

        public static const ALBUM_NY18_ALBUM18:String = "#ny:album/ny18/album18";

        public static const ALBUM_NY19_NAME:String = "#ny:album/ny19/name";

        public static const ALBUM_NY19_NEWYEAR_NAME:String = "#ny:album/ny19/NewYear/name";

        public static const ALBUM_NY19_NEWYEAR_BONUSNAME:String = "#ny:album/ny19/NewYear/bonusName";

        public static const ALBUM_NY19_ORIENTAL_NAME:String = "#ny:album/ny19/Oriental/name";

        public static const ALBUM_NY19_ORIENTAL_BONUSNAME:String = "#ny:album/ny19/Oriental/bonusName";

        public static const ALBUM_NY19_CHRISTMAS_NAME:String = "#ny:album/ny19/Christmas/name";

        public static const ALBUM_NY19_CHRISTMAS_BONUSNAME:String = "#ny:album/ny19/Christmas/bonusName";

        public static const ALBUM_NY19_FAIRYTALE_NAME:String = "#ny:album/ny19/Fairytale/name";

        public static const ALBUM_NY19_FAIRYTALE_BONUSNAME:String = "#ny:album/ny19/Fairytale/bonusName";

        public static const ALBUM_NY19_INFOTYPE_TIPS:String = "#ny:album/ny19/infotype/tips";

        public static const ALBUM_NY19_INFOTYPE_HOWTOGET:String = "#ny:album/ny19/infotype/howToGet";

        public static const ALBUM_NY20_INFOTYPE_BROKENDECORATION:String = "#ny:album/ny20/infotype/brokenDecoration";

        public static const ALBUM_NY20_INFOTIP_NEWYEARMISSIONS:String = "#ny:album/ny20/infotip/newYearMissions";

        public static const ALBUM_NY20_INFOTIP_CREATEINCOLLIDER:String = "#ny:album/ny20/infotip/createInCollider";

        public static const ALBUM_NY20_INFOTIP_BUYLOOTBOXES:String = "#ny:album/ny20/infotip/buyLootBoxes";

        public static const ALBUM_NY20_INFOTIP_INCORDECRATMLVL:String = "#ny:album/ny20/infotip/incOrDecrAtmLvl";

        public static const ALBUM_NY20_INFOTIP_DECCANBEBROKEN:String = "#ny:album/ny20/infotip/decCanBeBroken";

        public static const ALBUM_NY19_INFOTYPE_COLLECTANDINCRBONUS:String = "#ny:album/ny19/infotype/collectAndIncrBonus";

        public static const ALBUM_NY19_INFOTYPE_ALLRECDECOR:String = "#ny:album/ny19/infotype/allRecDecor";

        public static const ALBUM_NY19_INFOTYPE_RECEIVED:String = "#ny:album/ny19/infotype/received";

        public static const ALBUM_NY19_INFOTYPE_NOTRECEIVED:String = "#ny:album/ny19/infotype/notReceived";

        public static const ALBUM_NY19_INFOTYPE_INSTALLED:String = "#ny:album/ny19/infotype/installed";

        public static const ALBUM_NY20_INFOTIPMEGA_TITLE:String = "#ny:album/ny20/infotipMega/title";

        public static const ALBUM_NY20_INFOTIPMEGA_INCREASEBONUS:String = "#ny:album/ny20/infotipMega/increaseBonus";

        public static const ALBUM_NY20_INFOTIPMEGA_GIVEBONUS:String = "#ny:album/ny20/infotipMega/giveBonus";

        public static const ALBUM_NY20_INFOTIPMEGA_BONUSNOTACTIVE:String = "#ny:album/ny20/infotipMega/bonusNotActive";

        public static const ALBUM_NY20_INFOTIPMEGA_FORMOUNTMEGA:String = "#ny:album/ny20/infotipMega/forMountMega";

        public static const ALBUM_NY20_INFOTIPMEGA_BONUSACTIVE:String = "#ny:album/ny20/infotipMega/bonusActive";

        public static const ALBUM_NY20_INFOTIPMEGA_BONUSMEGAINFO:String = "#ny:album/ny20/infotipMega/bonusMegaInfo";

        public static const ALBUM_LEVELNAME:String = "#ny:album/levelName";

        public static const ALBUM_TOALBUM:String = "#ny:album/toAlbum";

        public static const ALBUM_NY20_MEGA_MOUNTINSLOT:String = "#ny:album/ny20/mega/mountInSlot";

        public static const ALBUM_NY20_PAGE20_BONUS:String = "#ny:album/ny20/page20/bonus";

        public static const ALBUM_NY20_PAGE20_BONUSBIG:String = "#ny:album/ny20/page20/bonusBig";

        public static const ALBUM_NY20_PAGE20_BONUSVALUE:String = "#ny:album/ny20/page20/bonusValue";

        public static const ALBUM_NY20_PAGE20_CREDITSAFTERBATTLE:String = "#ny:album/ny20/page20/creditsAfterBattle";

        public static const ALBUM_NY20_MEGA_CRASHED:String = "#ny:album/ny20/mega/crashed";

        public static const ALBUM_NY20_MEGA_INSTALLED:String = "#ny:album/ny20/mega/installed";

        public static const ALBUMHEADER_SETTINGS_CHRISTMAS:String = "#ny:albumHeader/settings/Christmas";

        public static const ALBUMHEADER_SETTINGS_ORIENTAL:String = "#ny:albumHeader/settings/Oriental";

        public static const ALBUMHEADER_SETTINGS_FAIRYTALE:String = "#ny:albumHeader/settings/Fairytale";

        public static const ALBUMHEADER_SETTINGS_NEWYEAR:String = "#ny:albumHeader/settings/NewYear";

        public static const ALBUMHEADER_SETTINGS_TRADITIONALWESTERN:String = "#ny:albumHeader/settings/traditionalWestern";

        public static const ALBUMHEADER_SETTINGS_ASIAN:String = "#ny:albumHeader/settings/asian";

        public static const ALBUMHEADER_SETTINGS_MODERNWESTERN:String = "#ny:albumHeader/settings/modernWestern";

        public static const ALBUMHEADER_SETTINGS_SOVIET:String = "#ny:albumHeader/settings/soviet";

        public static const ALBUMHEADER_SETTINGS_MEGA:String = "#ny:albumHeader/settings/Mega";

        public static const BREAKDECORATIONS_PARTS_AVAILABLE:String = "#ny:breakDecorations/parts/available";

        public static const BREAKDECORATIONS_SELECTEDALL:String = "#ny:breakDecorations/selectedAll";

        public static const BREAKDECORATIONS_PARTSTIP_PARTSENOUGH:String = "#ny:breakDecorations/partsTip/partsEnough";

        public static const BREAKDECORATIONS_PARTSTIP_PARTSNOTENOUGH:String = "#ny:breakDecorations/partsTip/partsNotEnough";

        public static const BREAKDECORATIONS_PARTSTIP_DECORATIONNOTSELECTED:String = "#ny:breakDecorations/partsTip/decorationNotSelected";

        public static const BREAKDECORATIONS_BREAKBTN_LABEL:String = "#ny:breakDecorations/breakBtn/label";

        public static const BREAKDECORATIONS_BREAKBTN_COUNT:String = "#ny:breakDecorations/breakBtn/count";

        public static const BREAKDECORATIONS_BREAKBTN_NOTSELECTED_TOOLTIP_BODY:String = "#ny:breakDecorations/breakBtn/notSelected/tooltip/body";

        public static const BREAKDECORATIONS_BREAKBTN_NOTAVAILABLE_TOOLTIP_BODY:String = "#ny:breakDecorations/breakBtn/notAvailable/tooltip/body";

        public static const BREAKDECORATIONS_DUMMY_TITLE:String = "#ny:breakDecorations/dummy/title";

        public static const BREAKDECORATIONS_DUMMY_DESCRIPTION_PART_1:String = "#ny:breakDecorations/dummy/description/part_1";

        public static const BREAKDECORATIONS_DUMMY_DESCRIPTION_PART_2:String = "#ny:breakDecorations/dummy/description/part_2";

        public static const BREAKDECORATIONS_DUMMY_DESCRIPTION_PART_3:String = "#ny:breakDecorations/dummy/description/part_3";

        public static const BREAKDECORATIONS_DUMMY_OPENQUESTSBTN:String = "#ny:breakDecorations/dummy/openQuestsBtn";

        public static const BREAKDECORATIONS_FILTER_NOTFOUND_TITLE:String = "#ny:breakDecorations/filter/notFound/title";

        public static const BREAKDECORATIONS_FILTER_NOTFOUND_DESCRIPTION:String = "#ny:breakDecorations/filter/notFound/description";

        public static const BREAKDECORATIONS_FILTER_RESETBTN:String = "#ny:breakDecorations/filter/resetBtn";

        public static const BREAKDECORATIONS_FILTER_RESULT:String = "#ny:breakDecorations/filter/result";

        public static const BREAKDECORATIONS_FILTERCOUNTER_TOOLTIP_HEADER:String = "#ny:breakDecorations/filterCounter/tooltip/header";

        public static const BREAKDECORATIONS_FILTERCOUNTER_TOOLTIP_BODY:String = "#ny:breakDecorations/filterCounter/tooltip/body";

        public static const BREAKDECORATIONS_FILTERCOUNTER_RESET_TOOLTIP_HEADER:String = "#ny:breakDecorations/filterCounter/reset/tooltip/header";

        public static const BREAKDECORATIONS_FILTERCOUNTER_RESET_TOOLTIP_BODY:String = "#ny:breakDecorations/filterCounter/reset/tooltip/body";

        public static const CONFIRMBREAKTOYS_TITLE:String = "#ny:confirmBreakToys/title";

        public static const CONFIRMBREAKTOYS_MESSAGE:String = "#ny:confirmBreakToys/message";

        public static const CONFIRMBREAKTOYS_MEGATOYSMESSAGE:String = "#ny:confirmBreakToys/megaToysMessage";

        public static const CONFIRMBREAKTOYS_SUBMIT:String = "#ny:confirmBreakToys/submit";

        public static const CONFIRMBREAKTOYS_CANCEL:String = "#ny:confirmBreakToys/cancel";

        public static const BREAKFILTERPOPOVER_TITLE:String = "#ny:breakFilterPopover/title";

        public static const BREAKFILTERPOPOVER_LEVEL:String = "#ny:breakFilterPopover/level";

        public static const BREAKFILTERPOPOVER_TYPE:String = "#ny:breakFilterPopover/type";

        public static const BREAKFILTERPOPOVER_MEGA:String = "#ny:breakFilterPopover/mega";

        public static const LEVELUPVIEW_CONGRATS:String = "#ny:levelUpView/congrats";

        public static const LEVELUPVIEW_REWARDS_TITLE:String = "#ny:levelUpView/rewards/title";

        public static const LEVELUPVIEW_REWARDS_SPECIALTITLE:String = "#ny:levelUpView/rewards/specialTitle";

        public static const LEVELUPVIEW_REWARDS_DESCRIPTION_PRIMARY:String = "#ny:levelUpView/rewards/description/primary";

        public static const LEVELUPVIEW_REWARDS_DESCRIPTION_PRIMARY_IFTALISMAN:String = "#ny:levelUpView/rewards/description/primary/ifTalisman";

        public static const LEVELUPVIEW_REWARDS_DESCRIPTION_IFTALISMAN:String = "#ny:levelUpView/rewards/description/ifTalisman";

        public static const LEVELUPVIEW_TOTANKSBTN_LABEL:String = "#ny:levelUpView/toTanksBtn/label";

        public static const LEVELUPVIEW_TOTALISMANSBTN_LABEL:String = "#ny:levelUpView/toTalismansBtn/label";

        public static const LEVELUPVIEW_OKBTN_LABEL:String = "#ny:levelUpView/okBtn/label";

        public static const SPECIALREWARDVIEW_REWARDS_TITLE:String = "#ny:specialRewardView/rewards/title";

        public static const SPECIALREWARDVIEW_REWARDS_DESCRIPTION:String = "#ny:specialRewardView/rewards/description";

        public static const SPECIALREWARDVIEW_RECRUITBTN_LABEL:String = "#ny:specialRewardView/recruitBtn/label";

        public static const SPECIALREWARDVIEW_OKBTN_LABEL:String = "#ny:specialRewardView/okBtn/label";

        public static const STYLEREWARDVIEW_REWARDS_TITLE:String = "#ny:styleRewardView/rewards/title";

        public static const STYLEREWARDVIEW_ORIENTAL_NY20:String = "#ny:styleRewardView/Oriental_ny20";

        public static const STYLEREWARDVIEW_NEWYEAR_NY20:String = "#ny:styleRewardView/NewYear_ny20";

        public static const STYLEREWARDVIEW_FAIRYTALE_NY20:String = "#ny:styleRewardView/Fairytale_ny20";

        public static const STYLEREWARDVIEW_CHRISTMAS_NY20:String = "#ny:styleRewardView/Christmas_ny20";

        public static const STYLEREWARDVIEW_ORIENTAL_NY19:String = "#ny:styleRewardView/Oriental_ny19";

        public static const STYLEREWARDVIEW_NEWYEAR_NY19:String = "#ny:styleRewardView/NewYear_ny19";

        public static const STYLEREWARDVIEW_FAIRYTALE_NY19:String = "#ny:styleRewardView/Fairytale_ny19";

        public static const STYLEREWARDVIEW_CHRISTMAS_NY19:String = "#ny:styleRewardView/Christmas_ny19";

        public static const STYLEREWARDVIEW_ASIAN_NY18:String = "#ny:styleRewardView/asian_ny18";

        public static const STYLEREWARDVIEW_SOVIET_NY18:String = "#ny:styleRewardView/soviet_ny18";

        public static const STYLEREWARDVIEW_TRADITIONALWESTERN_NY18:String = "#ny:styleRewardView/traditionalWestern_ny18";

        public static const STYLEREWARDVIEW_MODERNWESTERN_NY18:String = "#ny:styleRewardView/modernWestern_ny18";

        public static const STYLEREWARDVIEW_REWARDS_DESCRIPTION:String = "#ny:styleRewardView/rewards/description";

        public static const STYLEREWARDVIEW_OKBTN_LABEL:String = "#ny:styleRewardView/okBtn/label";

        public static const CRAFTVIEW_LEVELSSLIDER_TITLE:String = "#ny:craftView/levelsSlider/title";

        public static const CRAFTVIEW_LEVELSSLIDER_RANDOMBTN_TOOLTIP_HEADER:String = "#ny:craftView/levelsSlider/randomBtn/tooltip/header";

        public static const CRAFTVIEW_LEVELSSLIDER_LEVELBTN_TOOLTIP_BODY:String = "#ny:craftView/levelsSlider/levelBtn/tooltip/body";

        public static const CRAFTVIEW_SETTINGSSLIDER_TITLE:String = "#ny:craftView/settingsSlider/title";

        public static const CRAFTVIEW_CRAFTBTN:String = "#ny:craftView/craftBtn";

        public static const CRAFTVIEW_COST:String = "#ny:craftView/cost";

        public static const CRAFTVIEW_TYPEGROUP_TITLE:String = "#ny:craftView/typeGroup/title";

        public static const CRAFTVIEW_OBJECTTITLE_RANDOM:String = "#ny:craftView/objectTitle/random";

        public static const CRAFTVIEW_OBJECTTITLE_RANDOM_LEVEL:String = "#ny:craftView/objectTitle/random/level";

        public static const CRAFTVIEW_OBJECTTITLE_RANDOM_COLLECTION:String = "#ny:craftView/objectTitle/random/collection";

        public static const CRAFTVIEW_OBJECTTITLE_RANDOM_PLACE:String = "#ny:craftView/objectTitle/random/place";

        public static const CRAFTVIEW_OBJECTTITLE_FIR:String = "#ny:craftView/objectTitle/Fir";

        public static const CRAFTVIEW_OBJECTTITLE_TABLEFUL:String = "#ny:craftView/objectTitle/Tableful";

        public static const CRAFTVIEW_OBJECTTITLE_INSTALLATION:String = "#ny:craftView/objectTitle/Installation";

        public static const CRAFTVIEW_OBJECTTITLE_ILLUMINATION:String = "#ny:craftView/objectTitle/Illumination";

        public static const CRAFTVIEW_OBJECTTITLE_UPPER_RANDOM:String = "#ny:craftView/objectTitle/upper/random";

        public static const CRAFTVIEW_OBJECTTITLE_UPPER_FIR:String = "#ny:craftView/objectTitle/upper/Fir";

        public static const CRAFTVIEW_OBJECTTITLE_UPPER_TABLEFUL:String = "#ny:craftView/objectTitle/upper/Tableful";

        public static const CRAFTVIEW_OBJECTTITLE_UPPER_INSTALLATION:String = "#ny:craftView/objectTitle/upper/Installation";

        public static const CRAFTVIEW_OBJECTTITLE_UPPER_ILLUMINATION:String = "#ny:craftView/objectTitle/upper/Illumination";

        public static const CRAFTVIEW_MEGADEVICE_TITLE:String = "#ny:craftView/megaDevice/title";

        public static const CRAFTVIEW_ANTIDUPLICATOR_TITLE:String = "#ny:craftView/antiduplicator/title";

        public static const CRAFTVIEW_ANTIDUPLICATOR_SWITCH_OFF:String = "#ny:craftView/antiduplicator/switch/off";

        public static const CRAFTVIEW_ANTIDUPLICATOR_SWITCH_ON:String = "#ny:craftView/antiduplicator/switch/on";

        public static const CRAFTVIEW_ANTIDUPLICATOR_FILLERSLEFT:String = "#ny:craftView/antiduplicator/fillersLeft";

        public static const CRAFTVIEW_MONITOR_SHARDSLEFT:String = "#ny:craftView/monitor/shardsLeft";

        public static const CRAFTVIEW_MONITOR_LEVEL:String = "#ny:craftView/monitor/level";

        public static const CRAFTVIEW_MONITOR_SETTING:String = "#ny:craftView/monitor/setting";

        public static const CRAFTVIEW_MONITOR_TYPE:String = "#ny:craftView/monitor/type";

        public static const CRAFTVIEW_MONITOR_OBJECTTYPE:String = "#ny:craftView/monitor/objectType";

        public static const CRAFTVIEW_MONITOR_DUPLICATE_LABEL:String = "#ny:craftView/monitor/duplicate/label";

        public static const CRAFTVIEW_MONITOR_DUPLICATE_ON:String = "#ny:craftView/monitor/duplicate/on";

        public static const CRAFTVIEW_MONITOR_DUPLICATE_OFF:String = "#ny:craftView/monitor/duplicate/off";

        public static const CRAFTVIEW_MONITOR_COUNTMEGATOYS:String = "#ny:craftView/monitor/countMegaToys";

        public static const CRAFTVIEW_MONITOR_MEGATYPEDECOR:String = "#ny:craftView/monitor/megaTypeDecor";

        public static const CRAFTVIEW_MONITOR_REGULARTOY:String = "#ny:craftView/monitor/regularToy";

        public static const CRAFTVIEW_MONITOR_MEGATOY:String = "#ny:craftView/monitor/megaToy";

        public static const CRAFTVIEW_MONITOR_NOTENOUGHSHARDS_LABEL:String = "#ny:craftView/monitor/notEnoughShards/label";

        public static const CRAFTVIEW_MONITOR_NOTENOUGHSHARDS_DESCRIPTIONREGULAR:String = "#ny:craftView/monitor/notEnoughShards/descriptionRegular";

        public static const CRAFTVIEW_MONITOR_NOTENOUGHSHARDS_DESCRIPTIONMEGA:String = "#ny:craftView/monitor/notEnoughShards/descriptionMega";

        public static const CRAFTVIEW_MONITOR_NOTAVAILABLESHARDS_LABEL:String = "#ny:craftView/monitor/notAvailableShards/label";

        public static const CRAFTVIEW_MONITOR_NOTAVAILABLESHARDS_DESCRIPTION:String = "#ny:craftView/monitor/notAvailableShards/description";

        public static const CRAFTVIEW_MONITOR_NOTAVAILABLEFILLERS:String = "#ny:craftView/monitor/notAvailableFillers";

        public static const CRAFTVIEW_MONITOR_FULLREGULARSUBGROUP:String = "#ny:craftView/monitor/fullRegularSubGroup";

        public static const CRAFTVIEW_MONITOR_FULLREGULAR:String = "#ny:craftView/monitor/fullRegular";

        public static const CRAFTVIEW_MONITOR_FULLMEGA:String = "#ny:craftView/monitor/fullMega";

        public static const CRAFTVIEW_MONITOR_HASRANDOMPARAM:String = "#ny:craftView/monitor/hasRandomParam";

        public static const SETTINGS_CHRISTMAS:String = "#ny:settings/Christmas";

        public static const SETTINGS_ORIENTAL:String = "#ny:settings/Oriental";

        public static const SETTINGS_FAIRYTALE:String = "#ny:settings/Fairytale";

        public static const SETTINGS_NEWYEAR:String = "#ny:settings/NewYear";

        public static const SETTINGS_TRADITIONALWESTERN:String = "#ny:settings/traditionalWestern";

        public static const SETTINGS_ASIAN:String = "#ny:settings/asian";

        public static const SETTINGS_MODERNWESTERN:String = "#ny:settings/modernWestern";

        public static const SETTINGS_SOVIET:String = "#ny:settings/soviet";

        public static const SETTINGS_MEGA:String = "#ny:settings/Mega";

        public static const SETTINGSWITHCOLLECTION_CHRISTMAS:String = "#ny:settingsWithCollection/Christmas";

        public static const SETTINGSWITHCOLLECTION_ORIENTAL:String = "#ny:settingsWithCollection/Oriental";

        public static const SETTINGSWITHCOLLECTION_FAIRYTALE:String = "#ny:settingsWithCollection/Fairytale";

        public static const SETTINGSWITHCOLLECTION_NEWYEAR:String = "#ny:settingsWithCollection/NewYear";

        public static const SETTINGSWITHCOLLECTION_TRADITIONALWESTERN:String = "#ny:settingsWithCollection/traditionalWestern";

        public static const SETTINGSWITHCOLLECTION_ASIAN:String = "#ny:settingsWithCollection/asian";

        public static const SETTINGSWITHCOLLECTION_MODERNWESTERN:String = "#ny:settingsWithCollection/modernWestern";

        public static const SETTINGSWITHCOLLECTION_SOVIET:String = "#ny:settingsWithCollection/soviet";

        public static const SETTINGSWITHCOLLECTION_MEGA:String = "#ny:settingsWithCollection/Mega";

        public static const DECORATIONTYPES_RANDOM:String = "#ny:decorationTypes/random";

        public static const DECORATIONTYPES_TOP:String = "#ny:decorationTypes/top";

        public static const DECORATIONTYPES_BALL:String = "#ny:decorationTypes/ball";

        public static const DECORATIONTYPES_GARLAND:String = "#ny:decorationTypes/garland";

        public static const DECORATIONTYPES_FLOOR:String = "#ny:decorationTypes/floor";

        public static const DECORATIONTYPES_KITCHEN:String = "#ny:decorationTypes/kitchen";

        public static const DECORATIONTYPES_TABLE:String = "#ny:decorationTypes/table";

        public static const DECORATIONTYPES_SCULPTURE:String = "#ny:decorationTypes/sculpture";

        public static const DECORATIONTYPES_DECORATION:String = "#ny:decorationTypes/decoration";

        public static const DECORATIONTYPES_TREES:String = "#ny:decorationTypes/trees";

        public static const DECORATIONTYPES_GROUND_LIGHT:String = "#ny:decorationTypes/ground_light";

        public static const DECORATIONTYPES_HANGING:String = "#ny:decorationTypes/hanging";

        public static const DECORATIONTYPES_GIFT:String = "#ny:decorationTypes/gift";

        public static const DECORATIONTYPES_SNOWMAN:String = "#ny:decorationTypes/snowman";

        public static const DECORATIONTYPES_STREET_GARLAND:String = "#ny:decorationTypes/street_garland";

        public static const DECORATIONTYPES_HOUSE_DECORATION:String = "#ny:decorationTypes/house_decoration";

        public static const DECORATIONTYPES_HOUSE_LAMP:String = "#ny:decorationTypes/house_lamp";

        public static const DECORATIONTYPES_MEGA_FIR:String = "#ny:decorationTypes/mega_fir";

        public static const DECORATIONTYPES_TENT:String = "#ny:decorationTypes/tent";

        public static const DECORATIONTYPES_MEGA_TABLEFUL:String = "#ny:decorationTypes/mega_tableful";

        public static const DECORATIONTYPES_SNOW_ITEM:String = "#ny:decorationTypes/snow_item";

        public static const DECORATIONTYPES_MEGA_INSTALLATION:String = "#ny:decorationTypes/mega_installation";

        public static const DECORATIONTYPES_PYRO:String = "#ny:decorationTypes/pyro";

        public static const DECORATIONTYPES_MEGA_ILLUMINATION:String = "#ny:decorationTypes/mega_illumination";

        public static const DECORATIONTYPES_TYPEDISPLAY_RANDOM:String = "#ny:decorationTypes/typeDisplay/random";

        public static const DECORATIONTYPES_TYPEDISPLAY_TOP:String = "#ny:decorationTypes/typeDisplay/top";

        public static const DECORATIONTYPES_TYPEDISPLAY_BALL:String = "#ny:decorationTypes/typeDisplay/ball";

        public static const DECORATIONTYPES_TYPEDISPLAY_GARLAND:String = "#ny:decorationTypes/typeDisplay/garland";

        public static const DECORATIONTYPES_TYPEDISPLAY_FLOOR:String = "#ny:decorationTypes/typeDisplay/floor";

        public static const DECORATIONTYPES_TYPEDISPLAY_KITCHEN:String = "#ny:decorationTypes/typeDisplay/kitchen";

        public static const DECORATIONTYPES_TYPEDISPLAY_TABLE:String = "#ny:decorationTypes/typeDisplay/table";

        public static const DECORATIONTYPES_TYPEDISPLAY_SCULPTURE:String = "#ny:decorationTypes/typeDisplay/sculpture";

        public static const DECORATIONTYPES_TYPEDISPLAY_DECORATION:String = "#ny:decorationTypes/typeDisplay/decoration";

        public static const DECORATIONTYPES_TYPEDISPLAY_TREES:String = "#ny:decorationTypes/typeDisplay/trees";

        public static const DECORATIONTYPES_TYPEDISPLAY_GROUND_LIGHT:String = "#ny:decorationTypes/typeDisplay/ground_light";

        public static const LEVELSREWARDS_LOCK_TOOLTIP_HEADER:String = "#ny:levelsRewards/lock/tooltip/header";

        public static const LEVELSREWARDS_LOCK_TOOLTIP_DESCRIPTION:String = "#ny:levelsRewards/lock/tooltip/description";

        public static const LEVELSREWARDS_RENDERER_TANKWOMANBTN_LABEL:String = "#ny:levelsRewards/renderer/tankwomanBtn/label";

        public static const LEVELSREWARDS_RENDERER_TALISMANSELECTBTN_LABEL:String = "#ny:levelsRewards/renderer/talismanSelectBtn/label";

        public static const LEVELSREWARDS_RENDERER_TALISMANPREVIEWBTN_LABEL:String = "#ny:levelsRewards/renderer/talismanPreviewBtn/label";

        public static const LEVELSREWARDS_VEHICLESBTN_LABEL:String = "#ny:levelsRewards/vehiclesBtn/label";

        public static const LEVELSREWARDS_TANKWOMAN:String = "#ny:levelsRewards/tankWoman";

        public static const LEVELSREWARDS_TALISMANINFOTIP_TITLE:String = "#ny:levelsRewards/talismanInfotip/title";

        public static const LEVELSREWARDS_TALISMANINFOTIP_OPENACCESS:String = "#ny:levelsRewards/talismanInfotip/openAccess";

        public static const LEVELSREWARDS_TALISMANINFOTIP_ACTIVE:String = "#ny:levelsRewards/talismanInfotip/active";

        public static const LEVELSREWARDS_TALISMANINFOTIP_SNOWLADYINHANGAR:String = "#ny:levelsRewards/talismanInfotip/snowLadyInHangar";

        public static const LEVELSREWARDS_TALISMANINFOTIP_GIFTREMINDER:String = "#ny:levelsRewards/talismanInfotip/giftReminder";

        public static const LEVELSREWARDS_TALISMANINFOTIP_CHOOSESNOWLADY:String = "#ny:levelsRewards/talismanInfotip/chooseSnowLady";

        public static const LEVELSREWARDS_ALBUMBTN_LABEL:String = "#ny:levelsRewards/albumBtn/label";

        public static const LEVELSREWARDS_ALBUM_TOOLTIP_HEADER:String = "#ny:levelsRewards/album/tooltip/header";

        public static const LEVELSREWARDS_ALBUM_TOOLTIP_BODY:String = "#ny:levelsRewards/album/tooltip/body";

        public static const BONUSINFOTOOLTIP_TITLE:String = "#ny:bonusInfoTooltip/title";

        public static const BONUSINFOTOOLTIP_DESCRIPTION:String = "#ny:bonusInfoTooltip/description";

        public static const BONUSINFOTOOLTIP_HOWTO_TITLE:String = "#ny:bonusInfoTooltip/howTo/title";

        public static const BONUSINFOTOOLTIP_HOWTO_DESCRIPTION1:String = "#ny:bonusInfoTooltip/howTo/description1";

        public static const BONUSINFOTOOLTIP_HOWTO_DESCRIPTION2:String = "#ny:bonusInfoTooltip/howTo/description2";

        public static const BONUSINFOTOOLTIP_VALID_TITLE:String = "#ny:bonusInfoTooltip/valid/title";

        public static const BONUSINFOTOOLTIP_VALID_DESCRIPTION:String = "#ny:bonusInfoTooltip/valid/description";

        public static const BONUSINFOTOOLTIP_BOOSTER_XP:String = "#ny:bonusInfoTooltip/booster/xp";

        public static const BONUSINFOTOOLTIP_BOOSTER_FREE_XP:String = "#ny:bonusInfoTooltip/booster/free_xp";

        public static const BONUSINFOTOOLTIP_BOOSTER_CREW_XP:String = "#ny:bonusInfoTooltip/booster/crew_xp";

        public static const BONUSINFOTOOLTIP_BOOSTER_CREDITS:String = "#ny:bonusInfoTooltip/booster/credits";

        public static const BONUSINFOTOOLTIP_TANKIMAGE_HEADER:String = "#ny:bonusInfoTooltip/tankImage/header";

        public static const BONUSINFOTOOLTIP_TANKIMAGE_BODY:String = "#ny:bonusInfoTooltip/tankImage/body";

        public static const BONUSINFOTOOLTIP_NOBONUSES:String = "#ny:bonusInfoTooltip/noBonuses";

        public static const TOY18INFOTYPE_DECORLEVEL:String = "#ny:toy18Infotype/decorLevel";

        public static const TOY18INFOTYPE_COSTPARTS:String = "#ny:toy18Infotype/costParts";

        public static const STAMP_STAMPREWARD:String = "#ny:stamp/stampReward";

        public static const STAMP_PAGE18_GOTOREWARDS:String = "#ny:stamp/page18/goToRewards";

        public static const STAMP_PAGE19_GOTOREWARDS:String = "#ny:stamp/page19/goToRewards";

        public static const STAMP_PAGE20_GOTOREWARDS:String = "#ny:stamp/page20/goToRewards";

        public static const STAMP_PAGE20_MEGA:String = "#ny:stamp/page20/mega";

        public static const REWARDBLOCKINFOTYPE_TITLE:String = "#ny:rewardBlockInfotype/title";

        public static const REWARDBLOCKINFOTYPE_APPLYTO_ALL_TITLE:String = "#ny:rewardBlockInfotype/applyTo/all/title";

        public static const REWARDBLOCKINFOTYPE_APPLYTO_SELECTED_TITLE:String = "#ny:rewardBlockInfotype/applyTo/selected/title";

        public static const REWARDBLOCKINFOTYPE_STYLE_TITLE:String = "#ny:rewardBlockInfotype/style/title";

        public static const REWARDBLOCKINFOTYPE_NY18_STYLE_DESCRIPTION:String = "#ny:rewardBlockInfotype/ny18/style/description";

        public static const REWARDBLOCKINFOTYPE_NY18_LABELS_DESCRIPTION:String = "#ny:rewardBlockInfotype/ny18/labels/description";

        public static const REWARDBLOCKINFOTYPE_NY18_HEADER_DESCRIPTION:String = "#ny:rewardBlockInfotype/ny18/header/description";

        public static const REWARDBLOCKINFOTYPE_NY18_EMBLEM_DESCRIPTION:String = "#ny:rewardBlockInfotype/ny18/emblem/description";

        public static const REWARDBLOCKINFOTYPE_NY19_STYLE_DESCRIPTION:String = "#ny:rewardBlockInfotype/ny19/style/description";

        public static const REWARDBLOCKINFOTYPE_NY19_LABELS_DESCRIPTION:String = "#ny:rewardBlockInfotype/ny19/labels/description";

        public static const REWARDBLOCKINFOTYPE_NY19_EMBLEM_DESCRIPTION:String = "#ny:rewardBlockInfotype/ny19/emblem/description";

        public static const NOTIFICATION_SUSPEND:String = "#ny:notification/suspend";

        public static const NOTIFICATION_RESUME:String = "#ny:notification/resume";

        public static const NOTIFICATION_START:String = "#ny:notification/start";

        public static const NOTIFICATION_START_BUTTON:String = "#ny:notification/start/button";

        public static const NOTIFICATION_POSTEVENT:String = "#ny:notification/postEvent";

        public static const NOTIFICATION_FINISH:String = "#ny:notification/finish";

        public static const NOTIFICATION_REWARDSREMINDER:String = "#ny:notification/rewardsReminder";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_HEADER:String = "#ny:notification/collectionComplete/header";

        public static const NOTIFICATION_COLLECTIONCOMPLETE:String = "#ny:notification/collectionComplete";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_NAME_NEWYEAR:String = "#ny:notification/collectionComplete/name/newyear";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_NAME_CHRISTMAS:String = "#ny:notification/collectionComplete/name/christmas";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_NAME_ORIENTAL:String = "#ny:notification/collectionComplete/name/oriental";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_NAME_FAIRYTALE:String = "#ny:notification/collectionComplete/name/fairytale";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_NAME_MEGA:String = "#ny:notification/collectionComplete/name/mega";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_INSCRIPTION:String = "#ny:notification/collectionComplete/inscription";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_STYLE:String = "#ny:notification/collectionComplete/style";

        public static const NOTIFICATION_COLLECTIONCOMPLETE_EMBLEM:String = "#ny:notification/collectionComplete/emblem";

        public static const NOTIFICATION_LEVELUP_CONGRATS:String = "#ny:notification/levelUp/congrats";

        public static const NOTIFICATION_LEVELUP_REWARDS:String = "#ny:notification/levelUp/rewards";

        public static const NOTIFICATION_TALISMAN:String = "#ny:notification/talisman";

        public static const NOTIFICATION_FREETALISMAN:String = "#ny:notification/freeTalisman";

        public static const NOTIFICATION_GIFTTALISMAN:String = "#ny:notification/giftTalisman";

        public static const NOTIFICATION_DISABLEDFREETALISMAN:String = "#ny:notification/disabledFreeTalisman";

        public static const NOTIFICATION_DISABLEDGIFTTALISMAN:String = "#ny:notification/disabledGiftTalisman";

        public static const ALBUM_HISTORICFACTS_GETLEVEL_PART0:String = "#ny:album/historicFacts/getLevel/part0";

        public static const ALBUM_HISTORICFACTS_GETLEVEL_PART1:String = "#ny:album/historicFacts/getLevel/part1";

        public static const ALBUM_HISTORICFACTS_GETLEVEL_PART2:String = "#ny:album/historicFacts/getLevel/part2";

        public static const ALBUM_HISTORICFACTS_COUNTERTITLE:String = "#ny:album/historicFacts/counterTitle";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO1:String = "#ny:album/historicFacts/fact/title/no1";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO1:String = "#ny:album/historicFacts/fact/text/part0/no1";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO1:String = "#ny:album/historicFacts/fact/text/part1/no1";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO1:String = "#ny:album/historicFacts/fact/text/part2/no1";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO2:String = "#ny:album/historicFacts/fact/title/no2";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO2:String = "#ny:album/historicFacts/fact/text/part0/no2";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO2:String = "#ny:album/historicFacts/fact/text/part1/no2";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO2:String = "#ny:album/historicFacts/fact/text/part2/no2";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO3:String = "#ny:album/historicFacts/fact/title/no3";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO3:String = "#ny:album/historicFacts/fact/text/part0/no3";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO3:String = "#ny:album/historicFacts/fact/text/part1/no3";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO3:String = "#ny:album/historicFacts/fact/text/part2/no3";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO4:String = "#ny:album/historicFacts/fact/title/no4";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO4:String = "#ny:album/historicFacts/fact/text/part0/no4";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO4:String = "#ny:album/historicFacts/fact/text/part1/no4";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO4:String = "#ny:album/historicFacts/fact/text/part2/no4";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO5:String = "#ny:album/historicFacts/fact/title/no5";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO5:String = "#ny:album/historicFacts/fact/text/part0/no5";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO5:String = "#ny:album/historicFacts/fact/text/part1/no5";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO5:String = "#ny:album/historicFacts/fact/text/part2/no5";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO6:String = "#ny:album/historicFacts/fact/title/no6";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO6:String = "#ny:album/historicFacts/fact/text/part0/no6";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO6:String = "#ny:album/historicFacts/fact/text/part1/no6";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO6:String = "#ny:album/historicFacts/fact/text/part2/no6";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO7:String = "#ny:album/historicFacts/fact/title/no7";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO7:String = "#ny:album/historicFacts/fact/text/part0/no7";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO7:String = "#ny:album/historicFacts/fact/text/part1/no7";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO7:String = "#ny:album/historicFacts/fact/text/part2/no7";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO8:String = "#ny:album/historicFacts/fact/title/no8";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO8:String = "#ny:album/historicFacts/fact/text/part0/no8";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO8:String = "#ny:album/historicFacts/fact/text/part1/no8";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO8:String = "#ny:album/historicFacts/fact/text/part2/no8";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO9:String = "#ny:album/historicFacts/fact/title/no9";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO9:String = "#ny:album/historicFacts/fact/text/part0/no9";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO9:String = "#ny:album/historicFacts/fact/text/part1/no9";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO9:String = "#ny:album/historicFacts/fact/text/part2/no9";

        public static const ALBUM_HISTORICFACTS_FACT_TITLE_NO10:String = "#ny:album/historicFacts/fact/title/no10";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART0_NO10:String = "#ny:album/historicFacts/fact/text/part0/no10";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART1_NO10:String = "#ny:album/historicFacts/fact/text/part1/no10";

        public static const ALBUM_HISTORICFACTS_FACT_TEXT_PART2_NO10:String = "#ny:album/historicFacts/fact/text/part2/no10";

        public static const ALBUM_SETTINGTOOLTIP:String = "#ny:album/settingToolTip";

        public static const ALBUM_TIERTOOLTIP_LEVEL:String = "#ny:album/tierToolTip/level";

        public static const ALBUM_TIERTOOLTIP_LEVEL5:String = "#ny:album/tierToolTip/level5";

        public static const ALBUM_TIERTOOLTIP_LEVEL4:String = "#ny:album/tierToolTip/level4";

        public static const ALBUM_TIERTOOLTIP_LEVEL3:String = "#ny:album/tierToolTip/level3";

        public static const ALBUM_TIERTOOLTIP_LEVEL2:String = "#ny:album/tierToolTip/level2";

        public static const ALBUM_TIERTOOLTIP_LEVEL1:String = "#ny:album/tierToolTip/level1";

        public static const ALBUM_TIERTOOLTIP_BODY:String = "#ny:album/tierToolTip/body";

        public static const ALBUM_BONUSINFO18_BODY:String = "#ny:album/bonusInfo18/body";

        public static const ALBUM_BONUSINFO18_TAKENEWSTYLE:String = "#ny:album/bonusInfo18/takeNewStyle";

        public static const ALBUM_BUYCOLLECTION:String = "#ny:album/buyCollection";

        public static const FRAGMENTS_NAME:String = "#ny:fragments/name";

        public static const FRAGMENTS_TOOLTIP:String = "#ny:fragments/tooltip";

        public static const ALBUMBONUSTOOLTIP_HEADER:String = "#ny:albumBonusTooltip/header";

        public static const ALBUMBONUSTOOLTIP_DESCRIPTION:String = "#ny:albumBonusTooltip/description";

        public static const ALBUMBONUSTOOLTIP_LEVEL_LABEL:String = "#ny:albumBonusTooltip/level/label";

        public static const ALBUMBONUSTOOLTIP_BONUSVALUE_LABEL:String = "#ny:albumBonusTooltip/bonusValue/label";

        public static const ALBUMBONUSTOOLTIP_HOWTOUP_HEADER:String = "#ny:albumBonusTooltip/howToUp/header";

        public static const ALBUMBONUSTOOLTIP_HOWTOUP_TOYSCOUNT_HEADER:String = "#ny:albumBonusTooltip/howToUp/toysCount/header";

        public static const ALBUMBONUSTOOLTIP_HOWTOUP_TOYSCOUNT_MORE:String = "#ny:albumBonusTooltip/howToUp/toysCount/more";

        public static const ALBUMBONUSTOOLTIP_HOWTOUP_BONUSVALUE_HEADER:String = "#ny:albumBonusTooltip/howToUp/bonusValue/header";

        public static const ALBUMBONUSTOOLTIP_COUNTTOYS:String = "#ny:albumBonusTooltip/countToys";

        public static const ALBUM20_BONUSINFO:String = "#ny:album20/bonusInfo";

        public static const ALBUM20_BONUSCREDITS:String = "#ny:album20/bonusCredits";

        public static const ALBUM20_BONUS_CHRISTMAS:String = "#ny:album20/bonus/Christmas";

        public static const ALBUM20_BONUS_ORIENTAL:String = "#ny:album20/bonus/Oriental";

        public static const ALBUM20_BONUS_FAIRYTALE:String = "#ny:album20/bonus/Fairytale";

        public static const ALBUM20_BONUS_NEWYEAR:String = "#ny:album20/bonus/NewYear";

        public static const ALBUM20_BONUS_MEGA:String = "#ny:album20/bonus/Mega";

        public static const DIALOGS_BUYCOLLECTIONITEM_PRICE:String = "#ny:dialogs/buyCollectionItem/price";

        public static const DIALOGS_BUYCOLLECTIONITEM_BALANCE:String = "#ny:dialogs/buyCollectionItem/balance";

        public static const DIALOGS_BUYCOLLECTIONITEM_BTNBUY:String = "#ny:dialogs/buyCollectionItem/btnBuy";

        public static const DIALOGS_BUYCOLLECTIONITEM_BTNCANCEL:String = "#ny:dialogs/buyCollectionItem/btnCancel";

        public static const DIALOGS_BUYCOLLECTIONITEM_TITLE:String = "#ny:dialogs/buyCollectionItem/title";

        public static const DIALOGS_BUYFULLCOLLECTION_TITLE:String = "#ny:dialogs/buyFullCollection/title";

        public static const DIALOGS_SETVEHICLEBRANCH_BTNAPPLY:String = "#ny:dialogs/setVehicleBranch/btnApply";

        public static const DIALOGS_SETVEHICLEBRANCH_BTNSELECT:String = "#ny:dialogs/setVehicleBranch/btnSelect";

        public static const DIALOGS_SETVEHICLEBRANCH_BTNCANCEL:String = "#ny:dialogs/setVehicleBranch/btnCancel";

        public static const DIALOGS_SETVEHICLEBRANCH_PRICE:String = "#ny:dialogs/setVehicleBranch/price";

        public static const DIALOGS_SETVEHICLEBRANCH_TITLE:String = "#ny:dialogs/setVehicleBranch/title";

        public static const COLLECTIONSREWARD_ORIENTAL_NY20_HEADER:String = "#ny:collectionsReward/Oriental_ny20/header";

        public static const COLLECTIONSREWARD_NEWYEAR_NY20_HEADER:String = "#ny:collectionsReward/NewYear_ny20/header";

        public static const COLLECTIONSREWARD_FAIRYTALE_NY20_HEADER:String = "#ny:collectionsReward/Fairytale_ny20/header";

        public static const COLLECTIONSREWARD_CHRISTMAS_NY20_HEADER:String = "#ny:collectionsReward/Christmas_ny20/header";

        public static const COLLECTIONSREWARD_ORIENTAL_NY19_HEADER:String = "#ny:collectionsReward/Oriental_ny19/header";

        public static const COLLECTIONSREWARD_NEWYEAR_NY19_HEADER:String = "#ny:collectionsReward/NewYear_ny19/header";

        public static const COLLECTIONSREWARD_FAIRYTALE_NY19_HEADER:String = "#ny:collectionsReward/Fairytale_ny19/header";

        public static const COLLECTIONSREWARD_CHRISTMAS_NY19_HEADER:String = "#ny:collectionsReward/Christmas_ny19/header";

        public static const COLLECTIONSREWARD_ASIAN_NY18_HEADER:String = "#ny:collectionsReward/asian_ny18/header";

        public static const COLLECTIONSREWARD_SOVIET_NY18_HEADER:String = "#ny:collectionsReward/soviet_ny18/header";

        public static const COLLECTIONSREWARD_TRADITIONALWESTERN_NY18_HEADER:String = "#ny:collectionsReward/traditionalWestern_ny18/header";

        public static const COLLECTIONSREWARD_MODERNWESTERN_NY18_HEADER:String = "#ny:collectionsReward/modernWestern_ny18/header";

        public static const COLLECTIONSREWARD_PREVIEW:String = "#ny:collectionsReward/preview";

        public static const COLLECTIONSREWARD_PREVIEW_TOOLTIP:String = "#ny:collectionsReward/preview/tooltip";

        public static const COLLECTIONSREWARD_GETSTYLE:String = "#ny:collectionsReward/getStyle";

        public static const COLLECTIONSREWARD_TOCOLLECTIONBTN:String = "#ny:collectionsReward/toCollectionBtn";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_FAIRYTALE_NY20:String = "#ny:collectionsReward/stylePreview/Fairytale_ny20";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_ORIENTAL_NY20:String = "#ny:collectionsReward/stylePreview/Oriental_ny20";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_NEWYEAR_NY20:String = "#ny:collectionsReward/stylePreview/NewYear_ny20";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_CHRISTMAS_NY20:String = "#ny:collectionsReward/stylePreview/Christmas_ny20";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_FAIRYTALE_NY19:String = "#ny:collectionsReward/stylePreview/Fairytale_ny19";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_ORIENTAL_NY19:String = "#ny:collectionsReward/stylePreview/Oriental_ny19";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_NEWYEAR_NY19:String = "#ny:collectionsReward/stylePreview/NewYear_ny19";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_CHRISTMAS_NY19:String = "#ny:collectionsReward/stylePreview/Christmas_ny19";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_TRADITIONALWESTERN_NY18:String = "#ny:collectionsReward/stylePreview/traditionalWestern_ny18";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_ASIAN_NY18:String = "#ny:collectionsReward/stylePreview/asian_ny18";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_SOVIET_NY18:String = "#ny:collectionsReward/stylePreview/soviet_ny18";

        public static const COLLECTIONSREWARD_STYLEPREVIEW_MODERNWESTERN_NY18:String = "#ny:collectionsReward/stylePreview/modernWestern_ny18";

        public static const COLLECTIONSREWARD_STYLEPREVIEWACQUIRED:String = "#ny:collectionsReward/stylePreviewAcquired";

        public static const COLLECTIONSREWARD_FORNATIONS:String = "#ny:collectionsReward/forNations";

        public static const COLLECTIONSREWARD_FORLEVELS:String = "#ny:collectionsReward/forLevels";

        public static const COLLECTIONSREWARD_ALLNATIONS:String = "#ny:collectionsReward/allNations";

        public static const COLLECTIONSREWARD_ALLLEVELS:String = "#ny:collectionsReward/allLevels";

        public static const COLLECTIONSREWARD_STYLEACQUIRED:String = "#ny:collectionsReward/styleAcquired";

        public static const COLLECTIONSREWARD_ANDLEVELS:String = "#ny:collectionsReward/andLevels";

        public static const COLLECTIONSREWARD_COMMALEVELS:String = "#ny:collectionsReward/commaLevels";

        public static const COLLECTIONSREWARD_LASTLEVEL:String = "#ny:collectionsReward/lastLevel";

        public static const COLLECTIONSREWARD_BACKLABEL:String = "#ny:collectionsReward/backLabel";

        public static const COLLECTIONSREWARD_ISNOTENOUGHTOOLTIP:String = "#ny:collectionsReward/isNotEnoughTooltip";

        public static const COLLECTIONSREWARD_ISNOTMAXLEVELTOOLTIP:String = "#ny:collectionsReward/isNotMaxLevelTooltip";

        public static const WIDGETTOOLTIP_HEADER:String = "#ny:widgetTooltip/header";

        public static const WIDGETTOOLTIP_DESCRIPTION:String = "#ny:widgetTooltip/description";

        public static const WIDGETTOOLTIP_POINTS:String = "#ny:widgetTooltip/points";

        public static const WIDGETTOOLTIP_LEVEL:String = "#ny:widgetTooltip/level";

        public static const WIDGETTOOLTIP_MULTIPLIER:String = "#ny:widgetTooltip/multiplier";

        public static const WIDGETTOOLTIP_CREDITSBONUS:String = "#ny:widgetTooltip/creditsBonus";

        public static const WIDGETTOOLTIP_COLLECTIONBONUS:String = "#ny:widgetTooltip/collectionBonus";

        public static const WIDGETTOOLTIP_MEGABONUS:String = "#ny:widgetTooltip/megaBonus";

        public static const WIDGETTOOLTIP_LEVELSPATTERN:String = "#ny:widgetTooltip/levelsPattern";

        public static const TOTALBONUSWIDGET_PBBONUS:String = "#ny:totalBonusWidget/pbBonus";

        public static const TOTALBONUSTOOLTIP_HEADER:String = "#ny:totalBonusTooltip/header";

        public static const TOTALBONUSTOOLTIP_DESCRIPTION:String = "#ny:totalBonusTooltip/description";

        public static const TOTALBONUSTOOLTIP_POST_NY_DESCRIPTION:String = "#ny:totalBonusTooltip/post_ny/description";

        public static const TOTALBONUSTOOLTIP_PBBONUS:String = "#ny:totalBonusTooltip/pbBonus";

        public static const TOTALBONUSTOOLTIP_HOWTO_HEADER:String = "#ny:totalBonusTooltip/howTo/header";

        public static const TOTALBONUSTOOLTIP_HOWTO_MARKER:String = "#ny:totalBonusTooltip/howTo/marker";

        public static const TOTALBONUSTOOLTIP_HOWTO_DESCRIPTION1:String = "#ny:totalBonusTooltip/howTo/description1";

        public static const TOTALBONUSTOOLTIP_HOWTO_DESCRIPTION2:String = "#ny:totalBonusTooltip/howTo/description2";

        public static const TOTALBONUSTOOLTIP_HOWTO_DESCRIPTION3:String = "#ny:totalBonusTooltip/howTo/description3";

        public static const COLLECTIONBONUSTOOLTIP_HEADER:String = "#ny:collectionBonusTooltip/header";

        public static const COLLECTIONBONUSTOOLTIP_DESCRIPTION:String = "#ny:collectionBonusTooltip/description";

        public static const COLLECTIONBONUSTOOLTIP_TOYSCOUNT:String = "#ny:collectionBonusTooltip/toysCount";

        public static const COLLECTIONBONUSTOOLTIP_MORETHAN:String = "#ny:collectionBonusTooltip/moreThan";

        public static const COLLECTIONBONUSTOOLTIP_LEVELSPATTERN:String = "#ny:collectionBonusTooltip/levelsPattern";

        public static const VEHICLESVIEW_TITLE:String = "#ny:vehiclesView/title";

        public static const VEHICLESVIEW_SUBTITLE:String = "#ny:vehiclesView/subtitle";

        public static const VEHICLESVIEW_POST_NY_SUBTITLE:String = "#ny:vehiclesView/post_ny/subtitle";

        public static const VEHICLESVIEW_BONUSES_XPFACTOR:String = "#ny:vehiclesView/bonuses/xpFactor";

        public static const VEHICLESVIEW_BONUSES_FREEXPFACTOR:String = "#ny:vehiclesView/bonuses/freeXPFactor";

        public static const VEHICLESVIEW_BONUSES_TANKMENXPFACTOR:String = "#ny:vehiclesView/bonuses/tankmenXPFactor";

        public static const VEHICLESVIEW_BONUSFORMAT:String = "#ny:vehiclesView/bonusFormat";

        public static const VEHICLESVIEW_NEWYEARSTYLE:String = "#ny:vehiclesView/newYearStyle";

        public static const VEHICLESVIEW_VEHICLESLOT_CHANGETIMEOUT:String = "#ny:vehiclesView/vehicleSlot/changeTimeOut";

        public static const VEHICLESVIEW_VEHICLESLOT_CHANGETIMEOUTFORFREE:String = "#ny:vehiclesView/vehicleSlot/changeTimeOutForFree";

        public static const VEHICLESVIEW_VEHICLESLOT_SETCOOLDOWN:String = "#ny:vehiclesView/vehicleSlot/setCooldown";

        public static const VEHICLESVIEW_VEHICLESLOT_SETFOR:String = "#ny:vehiclesView/vehicleSlot/setFor";

        public static const VEHICLESVIEW_VEHICLESLOT_ADDVEHICLEFOR:String = "#ny:vehiclesView/vehicleSlot/addVehicleFor";

        public static const DIALOGS_SETVEHICLEBRANCH_ADDVEHICLETITLE:String = "#ny:dialogs/setVehicleBranch/addVehicleTitle";

        public static const VEHICLESVIEW_VEHICLESLOT_CHANGEDISABLED:String = "#ny:vehiclesView/vehicleSlot/changeDisabled";

        public static const VEHICLESVIEW_VEHICLESLOT_CHANGEDISABLEDDUETOSQUAD:String = "#ny:vehiclesView/vehicleSlot/changeDisabledDueToSquad";

        public static const VEHICLESVIEW_VEHICLESLOT_CHANGE:String = "#ny:vehiclesView/vehicleSlot/change";

        public static const VEHICLESVIEW_VEHICLESLOT_CHANGEFOR:String = "#ny:vehiclesView/vehicleSlot/changeFor";

        public static const VEHICLESVIEW_VEHICLESLOT_CHANGEFORFREE:String = "#ny:vehiclesView/vehicleSlot/changeForFree";

        public static const VEHICLESVIEW_VEHICLESLOT_CLEAR:String = "#ny:vehiclesView/vehicleSlot/clear";

        public static const VEHICLESVIEW_VEHICLESLOT_ADDVEHICLE:String = "#ny:vehiclesView/vehicleSlot/addVehicle";

        public static const VEHICLESVIEW_VEHICLESLOT_DISABLED:String = "#ny:vehiclesView/vehicleSlot/disabled";

        public static const VEHICLESVIEW_SELECTVEHICLEPOPOVER_HEADER:String = "#ny:vehiclesView/selectVehiclePopover/header";

        public static const VEHICLESVIEW_SELECTVEHICLEPOPOVER_DESCRIPTION:String = "#ny:vehiclesView/selectVehiclePopover/description";

        public static const VEHICLESVIEW_SELECTVEHICLEPOPOVER_POSTEVENT_DESCRIPTION:String = "#ny:vehiclesView/selectVehiclePopover/postEvent/description";

        public static const VEHICLESVIEW_SELECTVEHICLEPOPOVER_NOITEMS:String = "#ny:vehiclesView/selectVehiclePopover/noItems";

        public static const VEHICLESVIEW_INTRO_EVENT_HEADER:String = "#ny:vehiclesView/intro/event/header";

        public static const VEHICLESVIEW_INTRO_EVENT_DESC:String = "#ny:vehiclesView/intro/event/desc";

        public static const VEHICLESVIEW_INTRO_POSTEVENT_HEADER:String = "#ny:vehiclesView/intro/postEvent/header";

        public static const VEHICLESVIEW_INTRO_POSTEVENT_DESC:String = "#ny:vehiclesView/intro/postEvent/desc";

        public static const VEHICLESVIEW_INTRO_OKBTN_LABEL:String = "#ny:vehiclesView/intro/okBtn/label";

        public static const VEHICLESVIEW_INTRO_EVENTFIRST_HEADER:String = "#ny:vehiclesView/intro/eventFirst/header";

        public static const VEHICLESVIEW_INTRO_EVENTFIRST_DESC:String = "#ny:vehiclesView/intro/eventFirst/desc";

        public static const VEHICLESVIEW_INTRO_POSTEVENTFIRST_HEADER:String = "#ny:vehiclesView/intro/postEventFirst/header";

        public static const VEHICLESVIEW_INTRO_POSTEVENTFIRST_DESC:String = "#ny:vehiclesView/intro/postEventFirst/desc";

        public static const VEHICLESVIEW_INTRO_EVENTSECOND_HEADER:String = "#ny:vehiclesView/intro/eventSecond/header";

        public static const VEHICLESVIEW_INTRO_EVENTSECOND_DESC:String = "#ny:vehiclesView/intro/eventSecond/desc";

        public static const VEHICLESVIEW_INTRO_POSTEVENTSECOND_HEADER:String = "#ny:vehiclesView/intro/postEventSecond/header";

        public static const VEHICLESVIEW_INTRO_POSTEVENTSECOND_DESC:String = "#ny:vehiclesView/intro/postEventSecond/desc";

        public static const VEHICLESVIEW_INTRO_EVENTTHIRD_HEADER:String = "#ny:vehiclesView/intro/eventThird/header";

        public static const VEHICLESVIEW_INTRO_EVENTTHIRD_DESC:String = "#ny:vehiclesView/intro/eventThird/desc";

        public static const VEHICLESVIEW_INTRO_POSTEVENTTHIRD_HEADER:String = "#ny:vehiclesView/intro/postEventThird/header";

        public static const VEHICLESVIEW_INTRO_POSTEVENTTHIRD_DESC:String = "#ny:vehiclesView/intro/postEventThird/desc";

        public static const VEHICLESVIEW_BONUSTOOLTIP_TITLE:String = "#ny:vehiclesView/bonusTooltip/title";

        public static const VEHICLESVIEW_BONUSTOOLTIP_ABOUTNYSTYLE:String = "#ny:vehiclesView/bonusTooltip/aboutNyStyle";

        public static const VEHICLESVIEW_BONUSTOOLTIP_STYLEHASBONUS:String = "#ny:vehiclesView/bonusTooltip/styleHasBonus";

        public static const VEHICLEBONUSPANEL_XPFACTOR:String = "#ny:vehicleBonusPanel/xpFactor";

        public static const VEHICLEBONUSPANEL_FREEXPFACTOR:String = "#ny:vehicleBonusPanel/freeXPFactor";

        public static const VEHICLEBONUSPANEL_TANKMENXPFACTOR:String = "#ny:vehicleBonusPanel/tankmenXPFactor";

        public static const VEHICLEBONUSPANEL_TOOLTIP_SETCUSTOMIZATION_HEADER:String = "#ny:vehicleBonusPanel/tooltip/setCustomization/header";

        public static const VEHICLEBONUSPANEL_TOOLTIP_SETCUSTOMIZATION_BODY:String = "#ny:vehicleBonusPanel/tooltip/setCustomization/body";

        public static const VEHICLEBONUSPANEL_TOOLTIP_SETCUSTOMIZATION_DISABLED_HEADER:String = "#ny:vehicleBonusPanel/tooltip/setCustomization/disabled/header";

        public static const VEHICLEBONUSPANEL_TOOLTIP_SETCUSTOMIZATION_DISABLED_BODY:String = "#ny:vehicleBonusPanel/tooltip/setCustomization/disabled/body";

        public static const MEGATOYBONUSTOOLTIP_HEADER:String = "#ny:megaToyBonusTooltip/header";

        public static const MEGATOYBONUSTOOLTIP_DESCRIPTION:String = "#ny:megaToyBonusTooltip/description";

        public static const MEGATOYBONUSTOOLTIP_TOYTYPE:String = "#ny:megaToyBonusTooltip/toyType";

        public static const MEGATOYBONUSTOOLTIP_STATUS:String = "#ny:megaToyBonusTooltip/status";

        public static const MEGATOYBONUSTOOLTIP_BONUS:String = "#ny:megaToyBonusTooltip/bonus";

        public static const MEGATOYBONUSTOOLTIP_TOYTYPE_MEGA_FIR:String = "#ny:megaToyBonusTooltip/toyType/mega_fir";

        public static const MEGATOYBONUSTOOLTIP_TOYTYPE_MEGA_TABLEFUL:String = "#ny:megaToyBonusTooltip/toyType/mega_tableful";

        public static const MEGATOYBONUSTOOLTIP_TOYTYPE_MEGA_INSTALLATION:String = "#ny:megaToyBonusTooltip/toyType/mega_installation";

        public static const MEGATOYBONUSTOOLTIP_TOYTYPE_MEGA_ILLUMINATION:String = "#ny:megaToyBonusTooltip/toyType/mega_illumination";

        public static const MEGATOYBONUSTOOLTIP_STATUS_ABSENCE:String = "#ny:megaToyBonusTooltip/status/absence";

        public static const MEGATOYBONUSTOOLTIP_STATUS_NOTINSTALLED:String = "#ny:megaToyBonusTooltip/status/notInstalled";

        public static const MEGATOYBONUSTOOLTIP_STATUS_NOTINSTALLEDSTATUS:String = "#ny:megaToyBonusTooltip/status/notInstalledStatus";

        public static const MEGATOYBONUSTOOLTIP_STATUS_INSTALLED:String = "#ny:megaToyBonusTooltip/status/installed";

        public static const NEWYEARINFOVIEW_TITLE:String = "#ny:newYearInfoView/title";

        public static const NEWYEARINFOVIEW_ABOUT_TITLE:String = "#ny:newYearInfoView/about/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK1_TITLE:String = "#ny:newYearInfoView/about/block1/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK1_DESCRIPTION:String = "#ny:newYearInfoView/about/block1/description";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK2_TITLE:String = "#ny:newYearInfoView/about/block2/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK2_DESCRIPTION:String = "#ny:newYearInfoView/about/block2/description";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK3_TITLE:String = "#ny:newYearInfoView/about/block3/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK3_DESCRIPTION:String = "#ny:newYearInfoView/about/block3/description";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK4_TITLE:String = "#ny:newYearInfoView/about/block4/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK4_DESCRIPTION:String = "#ny:newYearInfoView/about/block4/description";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK5_TITLE:String = "#ny:newYearInfoView/about/block5/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK5_DESCRIPTION:String = "#ny:newYearInfoView/about/block5/description";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK6_TITLE:String = "#ny:newYearInfoView/about/block6/title";

        public static const NEWYEARINFOVIEW_ABOUT_BLOCK6_DESCRIPTION:String = "#ny:newYearInfoView/about/block6/description";

        public static const NEWYEARINFOVIEW_MAINREWARDS_TITLE:String = "#ny:newYearInfoView/mainRewards/title";

        public static const NEWYEARINFOVIEW_MAINREWARDS_LEVELS_TITLE:String = "#ny:newYearInfoView/mainRewards/levels/title";

        public static const NEWYEARINFOVIEW_MAINREWARDS_LEVELS_DESCRIPTION:String = "#ny:newYearInfoView/mainRewards/levels/description";

        public static const NEWYEARINFOVIEW_MAINREWARDS_LEVELS_BTN:String = "#ny:newYearInfoView/mainRewards/levels/btn";

        public static const NEWYEARINFOVIEW_MAINREWARDS_STYLES_TITLE:String = "#ny:newYearInfoView/mainRewards/styles/title";

        public static const NEWYEARINFOVIEW_MAINREWARDS_STYLES_DESCRIPTION:String = "#ny:newYearInfoView/mainRewards/styles/description";

        public static const NEWYEARINFOVIEW_MAINREWARDS_STYLES_BTN:String = "#ny:newYearInfoView/mainRewards/styles/btn";

        public static const NEWYEARINFOVIEW_MAINREWARDS_SMALLBOXES_TITLE:String = "#ny:newYearInfoView/mainRewards/smallBoxes/title";

        public static const NEWYEARINFOVIEW_MAINREWARDS_SMALLBOXES_DESCRIPTION:String = "#ny:newYearInfoView/mainRewards/smallBoxes/description";

        public static const NEWYEARINFOVIEW_MAINREWARDS_BIGBOXES_TITLE:String = "#ny:newYearInfoView/mainRewards/bigBoxes/title";

        public static const NEWYEARINFOVIEW_MAINREWARDS_BIGBOXES_DESCRIPTION:String = "#ny:newYearInfoView/mainRewards/bigBoxes/description";

        public static const NEWYEARINFOVIEW_MAINREWARDS_BIGBOXES_BTN:String = "#ny:newYearInfoView/mainRewards/bigBoxes/btn";

        public static const NEWYEARINFOVIEW_HOWTOGET_TITLE:String = "#ny:newYearInfoView/howToGet/title";

        public static const NEWYEARINFOVIEW_HOWTOGET_BLOCK1_TITLE:String = "#ny:newYearInfoView/howToGet/block1/title";

        public static const NEWYEARINFOVIEW_HOWTOGET_BLOCK1_DESCRIPTION:String = "#ny:newYearInfoView/howToGet/block1/description";

        public static const NEWYEARINFOVIEW_HOWTOGET_BLOCK2_TITLE:String = "#ny:newYearInfoView/howToGet/block2/title";

        public static const NEWYEARINFOVIEW_HOWTOGET_BLOCK2_DESCRIPTION:String = "#ny:newYearInfoView/howToGet/block2/description";

        public static const NEWYEARINFOVIEW_HOWTOGET_BLOCK3_TITLE:String = "#ny:newYearInfoView/howToGet/block3/title";

        public static const NEWYEARINFOVIEW_HOWTOGET_BLOCK3_DESCRIPTION:String = "#ny:newYearInfoView/howToGet/block3/description";

        public static const NEWYEARINFOVIEW_COLLECTIONS_TITLE:String = "#ny:newYearInfoView/collections/title";

        public static const NEWYEARINFOVIEW_COLLECTIONS_SUBTITLE:String = "#ny:newYearInfoView/collections/subtitle";

        public static const NEWYEARINFOVIEW_COLLECTIONS_BLOCK1_TITLE:String = "#ny:newYearInfoView/collections/block1/title";

        public static const NEWYEARINFOVIEW_COLLECTIONS_BLOCK1_DESCRIPTION:String = "#ny:newYearInfoView/collections/block1/description";

        public static const NEWYEARINFOVIEW_COLLECTIONS_BLOCK2_TITLE:String = "#ny:newYearInfoView/collections/block2/title";

        public static const NEWYEARINFOVIEW_COLLECTIONS_BLOCK2_DESCRIPTION:String = "#ny:newYearInfoView/collections/block2/description";

        public static const NEWYEARINFOVIEW_COLLECTIONS_BLOCK3_TITLE:String = "#ny:newYearInfoView/collections/block3/title";

        public static const NEWYEARINFOVIEW_COLLECTIONS_BLOCK3_DESCRIPTION:String = "#ny:newYearInfoView/collections/block3/description";

        public static const NEWYEARINFOVIEW_TANKS_TITLE:String = "#ny:newYearInfoView/tanks/title";

        public static const NEWYEARINFOVIEW_TANKS_BLOCK1_TITLE:String = "#ny:newYearInfoView/tanks/block1/title";

        public static const NEWYEARINFOVIEW_TANKS_BLOCK1_DESCRIPTION:String = "#ny:newYearInfoView/tanks/block1/description";

        public static const NEWYEARINFOVIEW_TANKS_BLOCK2_TITLE:String = "#ny:newYearInfoView/tanks/block2/title";

        public static const NEWYEARINFOVIEW_TANKS_BLOCK2_DESCRIPTION:String = "#ny:newYearInfoView/tanks/block2/description";

        public static const NEWYEARINFOVIEW_POSTNY_BLOCK1_TITLE:String = "#ny:newYearInfoView/postny/block1/title";

        public static const NEWYEARINFOVIEW_POSTNY_BLOCK1_DESCRIPTION:String = "#ny:newYearInfoView/postny/block1/description";

        public static const NEWYEARINFOVIEW_POSTNY_BLOCK2_TITLE:String = "#ny:newYearInfoView/postny/block2/title";

        public static const NEWYEARINFOVIEW_POSTNY_BLOCK2_DESCRIPTION:String = "#ny:newYearInfoView/postny/block2/description";

        public static const NEWYEARINFOVIEW_TALISMANS_TITLE:String = "#ny:newYearInfoView/talismans/title";

        public static const NEWYEARINFOVIEW_TALISMANS_BLOCK1_TITLE:String = "#ny:newYearInfoView/talismans/block1/title";

        public static const NEWYEARINFOVIEW_TALISMANS_BLOCK1_DESCRIPTION:String = "#ny:newYearInfoView/talismans/block1/description";

        public static const NEWYEARINFOVIEW_TALISMANS_BLOCK2_TITLE:String = "#ny:newYearInfoView/talismans/block2/title";

        public static const NEWYEARINFOVIEW_TALISMANS_BLOCK2_DESCRIPTION:String = "#ny:newYearInfoView/talismans/block2/description";

        public static const NEWYEARINFOVIEW_MEGA_TITLE:String = "#ny:newYearInfoView/mega/title";

        public static const NEWYEARINFOVIEW_MEGA_BLOCK1_TITLE:String = "#ny:newYearInfoView/mega/block1/title";

        public static const NEWYEARINFOVIEW_MEGA_BLOCK1_DESCRIPTION:String = "#ny:newYearInfoView/mega/block1/description";

        public static const NEWYEARINFOVIEW_MEGA_BLOCK2_TITLE:String = "#ny:newYearInfoView/mega/block2/title";

        public static const NEWYEARINFOVIEW_MEGA_BLOCK2_DESCRIPTION:String = "#ny:newYearInfoView/mega/block2/description";

        public static const NEWYEARINFOVIEW_COLLIDER_TITLE:String = "#ny:newYearInfoView/collider/title";

        public static const NEWYEARINFOVIEW_COLLIDER_BLOCK1_TITLE:String = "#ny:newYearInfoView/collider/block1/title";

        public static const NEWYEARINFOVIEW_COLLIDER_BLOCK1_DESCRIPTION:String = "#ny:newYearInfoView/collider/block1/description";

        public static const NEWYEARINFOVIEW_COLLIDER_BLOCK2_TITLE:String = "#ny:newYearInfoView/collider/block2/title";

        public static const NEWYEARINFOVIEW_COLLIDER_BLOCK2_DESCRIPTION:String = "#ny:newYearInfoView/collider/block2/description";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_HEADER:String = "#ny:newYearTalisman/talisman/select/header";

        public static const NEWYEARTALISMAN_TALISMAN_SELECTNEXT_HEADER:String = "#ny:newYearTalisman/talisman/selectNext/header";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_DESCRIPTION:String = "#ny:newYearTalisman/talisman/select/description";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_SELECTED:String = "#ny:newYearTalisman/talisman/select/selected";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_DESCRIPTION_NEWYEAR:String = "#ny:newYearTalisman/talisman/select/confirm/description/NewYear";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_DESCRIPTION_CHRISTMAS:String = "#ny:newYearTalisman/talisman/select/confirm/description/Christmas";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_DESCRIPTION_FAIRYTALE:String = "#ny:newYearTalisman/talisman/select/confirm/description/Fairytale";

        public static const NEWYEARTALISMAN_TALISMAN_SELECT_CONFIRM_DESCRIPTION_ORIENTAL:String = "#ny:newYearTalisman/talisman/select/confirm/description/Oriental";

        public static const NEWYEARTALISMAN_GIFT_SELECT_HEADER:String = "#ny:newYearTalisman/gift/select/header";

        public static const NEWYEARTALISMAN_GIFT_SELECT_DESCRIPTION:String = "#ny:newYearTalisman/gift/select/description";

        public static const NEWYEARTALISMAN_GIFT_CONGRAT_HEADER:String = "#ny:newYearTalisman/gift/congrat/header";

        public static const NEWYEARTALISMAN_TALISMAN_NUMBER0:String = "#ny:newYearTalisman/talisman/number0";

        public static const NEWYEARTALISMAN_TALISMAN_NUMBER1:String = "#ny:newYearTalisman/talisman/number1";

        public static const NEWYEARTALISMAN_TALISMAN_NUMBER2:String = "#ny:newYearTalisman/talisman/number2";

        public static const NEWYEARTALISMAN_TALISMAN_NUMBER3:String = "#ny:newYearTalisman/talisman/number3";

        public static const NEWYEARTALISMAN_TALISMAN_TYPEFROM_NEWYEAR:String = "#ny:newYearTalisman/talisman/typeFrom/NewYear";

        public static const NEWYEARTALISMAN_TALISMAN_TYPEFROM_CHRISTMAS:String = "#ny:newYearTalisman/talisman/typeFrom/Christmas";

        public static const NEWYEARTALISMAN_TALISMAN_TYPEFROM_FAIRYTALE:String = "#ny:newYearTalisman/talisman/typeFrom/Fairytale";

        public static const NEWYEARTALISMAN_TALISMAN_TYPEFROM_ORIENTAL:String = "#ny:newYearTalisman/talisman/typeFrom/Oriental";

        public static const NEWYEARTALISMAN_TALISMAN_TYPE_NEWYEAR:String = "#ny:newYearTalisman/talisman/type/NewYear";

        public static const NEWYEARTALISMAN_TALISMAN_TYPE_CHRISTMAS:String = "#ny:newYearTalisman/talisman/type/Christmas";

        public static const NEWYEARTALISMAN_TALISMAN_TYPE_FAIRYTALE:String = "#ny:newYearTalisman/talisman/type/Fairytale";

        public static const NEWYEARTALISMAN_TALISMAN_TYPE_ORIENTAL:String = "#ny:newYearTalisman/talisman/type/Oriental";

        public static const NEWYEARTALISMAN_SELECTBTN:String = "#ny:newYearTalisman/selectBtn";

        public static const NEWYEARTALISMAN_BACKBTN:String = "#ny:newYearTalisman/backBtn";

        public static const NEWYEARTALISMAN_GETBTN:String = "#ny:newYearTalisman/getBtn";

        public static const NEWYEARTALISMAN_BACKTREEBTN:String = "#ny:newYearTalisman/backTreeBtn";

        public static const NEWYEARTALISMAN_BACKTALISMANBTN:String = "#ny:newYearTalisman/backTalismanBtn";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_TITLE_DISABLED:String = "#ny:newYearTalismanGladeView/message/title/disabled";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_DESCRIPTION_DISABLED:String = "#ny:newYearTalismanGladeView/message/description/disabled";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_TITLE_SELECTNEW:String = "#ny:newYearTalismanGladeView/message/title/selectNew";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_TITLE_GIFTWAIT:String = "#ny:newYearTalismanGladeView/message/title/giftWait";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_DESCRIPTION_GIFTWAIT:String = "#ny:newYearTalismanGladeView/message/description/giftWait";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_TITLE_GIFTREADY:String = "#ny:newYearTalismanGladeView/message/title/giftReady";

        public static const NEWYEARTALISMANGLADEVIEW_MESSAGE_DESCRIPTION_GIFTREADY:String = "#ny:newYearTalismanGladeView/message/description/giftReady";

        public static const FILLERSTOOLTIP_HEADER:String = "#ny:fillersTooltip/header";

        public static const FILLERSTOOLTIP_DESCRIPTION:String = "#ny:fillersTooltip/description";

        public static const SYSTEMMESSAGE_INFOTOY:String = "#ny:systemMessage/infoToy";

        public static const SYSTEMMESSAGE_INFOCOLLECTION:String = "#ny:systemMessage/infoCollection";

        public static const SYSTEMMESSAGE_NY19:String = "#ny:systemMessage/ny19";

        public static const SYSTEMMESSAGE_NY18:String = "#ny:systemMessage/ny18";

        public static const NEWYEAR_SETVEHICLEBRANCH_CREDITS_SUCCESS:String = "#ny:newYear/setVehicleBranch/credits/success";

        public static const NEWYEAR_SETVEHICLEBRANCH_GOLD_SUCCESS:String = "#ny:newYear/setVehicleBranch/gold/success";

        public function NY()
        {
            super();
        }
    }
}
